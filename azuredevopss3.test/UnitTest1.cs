using NUnit.Framework;
using azuredevopss3.web.Operations;

namespace azuredevopss3.test
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [TestCase(3, 4, 7)]
        public void Test1(int a, int b, int expectedResult)
        {

            FinancialBasic fb = new FinancialBasic();

            Assert.AreEqual(expectedResult, fb.Suma(a, b));
        }
    }
}